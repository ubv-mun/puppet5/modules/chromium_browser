# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include chromium_browser
class chromium_browser (
  String        $package_ensure,
  Array[String] $package_name,
) {
  contain 'chromium_browser::install'
}
