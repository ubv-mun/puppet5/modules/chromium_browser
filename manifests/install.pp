# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include chromium_browser::install
class chromium_browser::install {
  package { $chromium_browser::package_name:
    ensure => $chromium_browser::package_ensure,
  }
}
